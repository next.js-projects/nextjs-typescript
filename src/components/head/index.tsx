import Head from 'next/head'
import { ReactChildren } from 'react';

export interface HeadProps {
  title?: string;
  children?: ReactChildren;
}

const head: React.FC<HeadProps> = ({title, children}) => {
  return (
    <Head>
      <title>{title || 'Pandemusic'}</title>
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="/favicon.ico" type="image/x-icon" />
      <meta name="msapplication-TileColor" content="#ffffff" />
      <meta name="theme-color" content="#ffffff" />
      {children}
    </Head>
  )
}

export default head