import React, { useEffect, ReactChildren } from 'react'

// Context
import { useContext } from 'react'
import { ThemeContext } from '../services/contexts/theme'

// // Style Components
import { ThemeProvider } from "styled-components"
import GlobalStyle from "../theme/globalStyle"
import ClassesGlobalStyles from "../theme/classes"
import FontGlobal from '../theme/fonts'

export interface LayoutProps {
  children: ReactChildren
}

const Layout: React.FC = ({children}) => {

  const context = useContext(ThemeContext)
  
  useEffect(() => {
    const nextBtn = document.querySelector('#__next-prerender-indicator')
    nextBtn && nextBtn.remove()
  }, [])

  return (
    <ThemeProvider theme={{name: context.theme}}>
      {children}
      { ClassesGlobalStyles.map((ClassesGlobalStyle, index) => <ClassesGlobalStyle key={index} />) }
      <GlobalStyle />
      <FontGlobal.Faces />
    </ThemeProvider>
  )

}

export default Layout