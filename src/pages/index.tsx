// Context
import React, { useContext } from 'react'
import { ThemeContext } from '../services/contexts/theme'

// Components
import Head from '../components/head'

import Style from './style'

export default () => {

  const context = useContext(ThemeContext)
  
  return (
    <Style>
      <Head />
      <a href="#" className="btn-link title" onClick={() => context.toogleTheme && context.toogleTheme()}>Home</a>
      <ul>
        <li>(X) services folder </li>
        <li>(X) Add styled-component</li>
        <li>(X) theme folder and adjusts to TypeScritp</li>
        <li>(X) Props _app.tsx</li>
        <li>(X) Add ThemeContextProvider (Context) in _app.tsx</li>
        <li>(X) Layout.tsx with themes</li>
        <li>(X) Theme default adjusts (rem, fonts, colors)</li>
        <li>(X) public folder (favicon, assets)</li>
        <li>(X) head of pages (config: title, favicon)</li>
        <li>try build project</li>
        <li>Git Nextjs TS</li>
        <li>Git Pandemusic</li>
      </ul>
    </Style>
  )
}