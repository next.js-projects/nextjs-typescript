import { GlobalStyleProps } from '../globalStyle'
import { createGlobalStyle, GlobalStyleComponent, DefaultTheme } from "styled-components"

import fonts from '../fonts'
import myTheme from '../index'

const texts: GlobalStyleComponent<GlobalStyleProps, DefaultTheme> = createGlobalStyle`

  :root {
    font-size: 60%;
    font-family: ${fonts.types.descrition};
    color: ${props => myTheme.colors[props.theme.name].secundary};
  }

  @media (min-width: 700px) {
    :root {
      font-size: 62.5%;
    }
  }

  body {
    font-size: 1.6rem;
  }

  .title {
    font-size: 5rem;
    font-family: ${fonts.types.title};
    color: ${props => myTheme.colors[props.theme.name].secundary};

    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: ${props => myTheme.colors[props.theme.name].primary};
    font-family: Germania One;
    text-shadow: 0 0 2rem ${props => myTheme.colors[props.theme.name].primary};
  }

  h3 {
    font-size: 3rem;
  }

  em {
    font-style: normal;
  }

`

export default texts