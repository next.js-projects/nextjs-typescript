import { GlobalStyleProps } from '../globalStyle'
import { createGlobalStyle, GlobalStyleComponent, DefaultTheme } from "styled-components"

import colors from "../colors";

const buttons: GlobalStyleComponent<GlobalStyleProps, DefaultTheme> = createGlobalStyle`

  a { text-decoration: none; cursor: pointer; }

  .btn-link {
    padding: 10px 15px;
    background-color: ${colors.matterhorn};
    color: ${colors.white};
    border-radius: 5px;
    box-shadow: 1px 1px 1px 1px ${colors.matterhorn}aa;

    transition: .25s;
  }

  .btn-link:hover {
    background-color: ${colors.white};
    color: ${colors.matterhorn};
  }

  .btn-link.disabled {
    cursor: default;
    pointer-events: none;
    color: ${colors.white}7a;
    background-color: ${colors.matterhorn};
  }

  .btn-link.selected {
    cursor: default;
    pointer-events: none;
    color: ${colors.lynch};
    background-color: ${colors.matterhorn};
  }

`

export default buttons