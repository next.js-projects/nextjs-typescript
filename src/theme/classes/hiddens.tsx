import { createGlobalStyle, DefaultTheme, GlobalStyleComponent } from "styled-components"

 const hiddens: GlobalStyleComponent<any, DefaultTheme> = createGlobalStyle`

  [class*='-desktop'] {
    display: none;  
  }

  #__next-prerender-indicator, {
    display: none !important;
  }

`

export default hiddens