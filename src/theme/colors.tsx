/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

let colors = {
  lynch: '#5c7689',
  french_pass: '#a3d1e2',
  green_waterloo: '#161710',
  white: '#ffffff',
  matterhorn: '#545454'
}

export default {
  ...colors,

  main: {
    primary: colors.french_pass,
    secundary: colors.white,

    background: colors.lynch,
  },

  dark: {
    primary: colors.french_pass,
    secundary: colors.green_waterloo,

    background: colors.lynch,
  }
};
