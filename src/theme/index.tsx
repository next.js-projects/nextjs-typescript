import colors from "./colors"
import fonts from "./fonts"
import classes from "./classes"

const theme: any = {
  colors,
  fonts,
  classes,
}

export default theme