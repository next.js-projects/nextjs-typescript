export interface FontsProps {
  name: string;
  url: Array<string>;
  type?: string;
}

export default [
  {
    name: "Germania One",
    url: ['/assets/fonts/Germania_One/GermaniaOne-Regular.ttf'],
    type: "title",
  },
  {
    name: "Roboto",
    url: ['/assets/fonts/Roboto/Roboto-Regular.ttf'],
    type: "description",
  },
];