import { createGlobalStyle, GlobalStyleComponent, DefaultTheme } from "styled-components"
import myTheme from './index'

export interface GlobalStyleProps {
  theme: {
    name: string;
  }
}

const globalStyle: GlobalStyleComponent<GlobalStyleProps, DefaultTheme> = createGlobalStyle`

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  html, body, #root {
    height: 100vh;
  }

  body {    
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    background-color: ${props => myTheme.colors[props.theme.name].background} !important;

    overflow-x: hidden;
  }

  .container {
    max-width: 1200px !important;
    margin: 0 auto;
  }

  .overlay {
    height: inherit;
    width: inherit;
  }

  ul { list-style: none; }

  img, picture, video, embed {
    max-width: 100%;
  }
`

export default globalStyle